/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   spec_p.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 15:44:42 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/27 15:44:44 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	spec_p(t_format *frmt, va_list prsd_lst, char code, char *str)
{
	if (code == 'p')
	{
		str[frmt->n[0]++] = '0';
		str[frmt->n[0]++] = 'x';
		frmt->v.li = (long)va_arg(prsd_lst, void *);
		frmt->out = &str[frmt->n[0]];
		li2b(frmt, 'x');
	}
}
