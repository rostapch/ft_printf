/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   output.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 15:43:17 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/27 15:43:20 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		putn(const char *str, size_t n, t_format *frmt)
{
	if (0 < (long long)(n))
	{
		write(1, str, n);
		frmt->nchar += n;
		return (1);
	}
	else
		return (0);
}

void	pad(const char ch, size_t n, t_format *frmt)
{
	char	*pad;

	if (0 < (long long)n)
	{
		pad = ft_strnew(n);
		if (!pad)
			return ;
		ft_memset(pad, ch, n);
		putn(pad, n, frmt);
		ft_strdel(&pad);
	}
}

void	output(t_format *frmt, char *str)
{
	frmt->width -= OUT_W;
	if (!(frmt->flags & MINUS))
		pad(' ', frmt->width, frmt);
	putn(str, frmt->n[0], frmt);
	pad('0', frmt->nz[0], frmt);
	putn(frmt->out, frmt->n[1], frmt);
	pad('0', frmt->nz[1], frmt);
	putn(frmt->out + frmt->n[1], frmt->n[2], frmt);
	pad('0', frmt->nz[2], frmt);
	if (frmt->flags & MINUS)
		pad(' ', frmt->width, frmt);
}
