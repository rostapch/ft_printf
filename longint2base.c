/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   longint2base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 15:43:01 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/27 15:43:02 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	count_base(char arg)
{
	int base;

	base = (ft_strchr("oO", arg)) ? 8 : 0;
	base = (base == 0 && ft_strchr("xX", arg)) ? 16 : base;
	base = (base == 0 && arg == 'b') ? 2 : base;
	base = (base == 0) ? 10 : base;
	return (base);
}

void		li2b(t_format *frmt, char arg)
{
	int		base;
	char	*ac;
	t_ull	uval;
	char	*tmp;

	base = count_base(arg);
	uval = frmt->v.li;
	if (ft_strchr("di", arg) && (t_ll)frmt->v.li < 0)
		uval = -uval;
	ac = AC_INIT;
	if (arg == 'X')
		CLEAN;
	frmt->n[1] = (int)ft_strlen(ac);
	ft_memcpy(frmt->out, ac, frmt->n[1] + 1);
	ft_strdel(&ac);
	if (frmt->n[1] < frmt->prec)
		frmt->nz[0] = frmt->prec - frmt->n[1];
	if (frmt->prec < 0 && CHECK_MZ && CHECK_N_NZ)
		frmt->nz[0] += base;
}
