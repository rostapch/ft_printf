/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   spec_c_lc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 15:44:09 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/27 15:44:11 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	spec_c(t_format *frmt, va_list parsed_lst, char specifier, char *ac)
{
	int		pad;

	if (specifier == 'c' && frmt->qual != l)
	{
		frmt->out = &ac[frmt->n[0]];
		frmt->out[frmt->n[1]++] = va_arg(parsed_lst, int);
		pad = 0;
		if ((frmt->flags & (ZERO | MINUS)) == ZERO && 0 <
			(pad = frmt->width - frmt->n[0] - frmt->nz[0] - frmt->n[1]))
			frmt->nz[0] += pad;
	}
	else if (specifier == 'c')
		spec_lc(frmt, parsed_lst, 'C');
}

void	spec_lc(t_format *frmt, va_list parsed_lst, char specifier)
{
	wchar_t	wc;
	int		pad;

	if (specifier == 'C')
	{
		wc = va_arg(parsed_lst, wchar_t);
		frmt->n[1] = 1;
		frmt->out = ft_strnew(1);
		frmt->out[0] = (char)wc;
		pad = 0;
		if (LC_MZ && LC_N_NZ)
			frmt->nz[0] += pad;
	}
}
