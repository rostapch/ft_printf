/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 15:41:10 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/27 15:41:12 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_FT_PRINTF_H
# define FT_PRINTF_FT_PRINTF_H
# include <stdlib.h>
# include <stdarg.h>
# include <unistd.h>
# include <inttypes.h>
# include "libft/libft.h"

# define ABS (x) ((x < 0) ? -x : x)

# define SPACE			0x01
# define PLUS			0x02
# define MINUS			0x04
# define NO				0x08
# define ZERO			0x10

# define N_HH_OK ((*va_arg(prsd_lst, char *)) = frmt->nchar)
# define N_HH_KO ((*va_arg(prsd_lst, short *)) = frmt->nchar)
# define N_LL_OK ((*va_arg(prsd_lst, long *)) = frmt->nchar)
# define N_LL_KO ((*va_arg(prsd_lst, t_ll *)) = frmt->nchar)
# define N_JZ_OK ((*va_arg(prsd_lst, intmax_t *)) = frmt->nchar)
# define N_JZ_KO ((*va_arg(prsd_lst, ssize_t *)) = frmt->nchar)
# define CLEAN {tmp=ac;ac=ft_str2upper(tmp);ft_strdel(&tmp);}
# define AC_INIT (frmt->prec||uval)?ull2ascii_base(uval,base):ft_strnew(1)
# define CHECK_MZ ((frmt->flags & (MINUS | ZERO)) == ZERO)
# define CHECK_N_NZ (0<(base=frmt->width-frmt->n[0]-frmt->nz[0]-frmt->n[1]))
# define OUT_N (frmt->n[0]+frmt->n[1]+frmt->n[2])
# define OUT_NZ (frmt->nz[0]+frmt->nz[1]+frmt->nz[2])
# define OUT_W (OUT_N + OUT_NZ)
# define SKIP_RET (*ret!='{'&&*ret!='%'&&*ret)
# define PUTN_RET (!putn(str, s - str, &frmt) && (*s) != '%' && (*s) != '{')
# define Q_L_LL (frmt->qual == l || frmt->qual == ll)
# define Q_H_HH (frmt->qual == h || frmt->qual == hh)
# define Q_J_Z (frmt->qual == j || frmt->qual == z)
# define BOUX_OU ((frmt->flags & NO) && code != 'u')
# define BOUX_SZ (((code=='o'&&!(frmt->v.li)&&(frmt->prec==0))||frmt->v.li))
# define LC_MZ ((frmt->flags & (ZERO | MINUS)) == ZERO)
# define LC_N_NZ (0<(pad=frmt->width-frmt->n[0]-frmt->nz[0]-frmt->n[1]))
# define DI_L_LL (frmt->qual == l || frmt->qual == ll)
# define DI_H_HH (frmt->qual == h || frmt->qual == hh)
# define DI_J_Z (frmt->qual == j || frmt->qual == z)
# define CHECK(prec, n)	(prec > 0 && prec < n)
# define S_ZM ((frmt->flags & (ZERO | MINUS)) == ZERO)
# define S_N_NZ (0<(pad=frmt->width-frmt->n[0]-frmt->nz[0]-frmt->n[1]))
# define LS_ZM ((frmt->flags & (ZERO | MINUS)) == ZERO)
# define LS_N_NZ (0<(pad=frmt->width-frmt->n[0]-frmt->nz[0]-frmt->n[1]))
# define SD_ZM ((frmt->flags & (ZERO | MINUS)) == ZERO)
# define SD_N_NZ (0<(pad=frmt->width-frmt->n[0]-frmt->nz[0]-frmt->n[1]))
# define CHECK_WCHAR ((wchr>=0&&wchr<=55295)||(wchr>=57344&&wchr<=1114111))
# define FIRST_BYTE ((unsigned char)wchr)
# define SECOND_BYTE (192 | (wchr >> (move * 6) & 15))
# define THIRD_BYTE (224 | (wchr >> (move * 6) & 7))
# define FOURTH_BYTE (240 | (wchr >> (move * 6) & 3))

static const unsigned int	g_flags_def[] = {MINUS, PLUS, SPACE, NO, ZERO, 0};

typedef unsigned long long	t_ull;
typedef unsigned long		t_ul;
typedef long long			t_ll;
typedef unsigned char		t_uchar;

typedef enum				e_len
{
	nolen = 0,
	hh = 1,
	h = 2,
	l = 3,
	ll = 4,
	j = 5,
	z = 6,
	L = 7
}							t_len;

typedef struct				s_format
{
	union					u_v
	{
		t_ll				li;
		long double			ld;
	}						v;
	char					*out;
	int						n[3];
	int						nz[3];
	int						prec;
	int						width;
	int						nchar;
	unsigned int			flags;
	t_len					qual;
}							t_format;

int							putn(const char *fmt, size_t n, t_format *frmt);
void						pad(const char ch, size_t n, t_format *frmt);
void						parse_flags(const char **s, t_format *frmt);
int							get_numb(const char **s, va_list ap);
void						get_width_arg(const char **s, t_format *frmt,
												va_list ap);
void						get_prec_arg(const char **s, t_format *x,
											va_list ap);
void						parse_length(const char **s, t_format *frmt);
int							parse_number(const char **s);

int							ft_printf_in(const char *fmt, va_list ap);
int							ft_printf(char *fmt, ...);
void						putfld(t_format *frmt, va_list pap, char code,
										char *ac);

void						li2b(t_format *frmt, char code);

void						spec_lc(t_format *frmt, va_list pap, char code);
void						spec_ls(t_format *frmt, va_list pap, char code);
void						spec_s(t_format *frmt, va_list pap, char code);
void						spec_c(t_format *frmt, va_list pap, char code,
									char *ac);
void						spec_di(t_format *frmt, va_list pap, char code,
									char *ac);
void						bin_o_u_x(t_format *frmt, va_list pap, char code,
									char *ac);
void						spec_p(t_format *frmt, va_list pap, char code,
									char *ac);
void						spec_n(t_format *frmt, va_list pap, char code);
void						spec_ldou(t_format *frmt, va_list pap, char code,
									char *ac);
void						spec_undef(t_format *frmt, char code, char *ac);
char						*ft_str2upper(char *ac);
void						output(t_format *frmt, char *ac);

char						*ll2ascii_base(t_ll value, int base);
int							ft_islower(int c);
int							ft_isupper(int c);
int							base_size(t_ll n, int base);
char						base_char(int i);
char						*ull2ascii_base(t_ull value, int base);

int							wchar2multibyte(char *str, wchar_t chr);
size_t						wcharstr2mb(char **dest, wchar_t *wstr);
size_t						wcharstrn2mb(char **dest, wchar_t *wstr,
							size_t len);

#endif
