/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   some_stuff.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 15:43:46 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/27 15:43:50 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*ft_str2upper(char *str)
{
	char	*temp;
	size_t	length;
	int		i;

	length = ft_strlen(str);
	i = 0;
	if (!(temp = ft_strnew(length)))
		return (NULL);
	while (length--)
	{
		*(temp + i) = (char)ft_toupper(*(str + i));
		i++;
	}
	return (temp);
}

int		parse_number(const char **s)
{
	int ret;

	ret = 0;
	while (ft_isdigit(**s))
	{
		ret = ret * 10 + **s - '0';
		(*s)++;
	}
	return (ret);
}

void	get_width_arg(const char **s, t_format *frmt, va_list str)
{
	frmt->width = get_numb(s, str);
	if (frmt->width < 0)
	{
		frmt->width = -frmt->width;
		frmt->flags |= MINUS;
	}
}
