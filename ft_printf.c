/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 15:41:02 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/27 15:41:05 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_printf(char *str, ...)
{
	int		ret;
	va_list	lst;

	va_start(lst, str);
	ret = ft_printf_in(str, lst);
	va_end(lst);
	return (ret);
}
