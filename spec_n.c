/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   spec_n.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 15:44:35 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/27 15:44:37 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	spec_n(t_format *frmt, va_list prsd_lst, char code)
{
	if (code == 'n')
	{
		if (frmt->qual == h || frmt->qual == hh)
			(frmt->qual == hh) ? N_HH_OK : N_HH_KO;
		else if (frmt->qual == l || frmt->qual == ll)
			(frmt->qual == ll) ? N_LL_OK : N_LL_KO;
		else if (frmt->qual == j || frmt->qual == z)
			(frmt->qual == j) ? N_JZ_OK : N_JZ_KO;
		else
			(*va_arg(prsd_lst, int *)) = frmt->nchar;
	}
}
