/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   spec_di.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 15:44:16 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/27 15:44:18 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	spec_di(t_format *frmt, va_list parsed_lst, char specifier, char *ac)
{
	if (ft_strchr("di", specifier))
	{
		if (DI_L_LL)
			frmt->v.li = (frmt->qual == ll) ?
			va_arg(parsed_lst, long long) : va_arg(parsed_lst, long);
		else if (DI_H_HH)
			frmt->v.li = (frmt->qual == hh) ?
			(char)va_arg(parsed_lst, int) : (short)va_arg(parsed_lst, int);
		else if (DI_J_Z)
			frmt->v.li = (frmt->qual == j) ? va_arg(parsed_lst, intmax_t)
											: va_arg(parsed_lst, ssize_t);
		else
			frmt->v.li = va_arg(parsed_lst, int);
		if ((long long)frmt->v.li < 0)
			ac[frmt->n[0]++] = '-';
		else if (frmt->flags & PLUS)
			ac[frmt->n[0]++] = '+';
		else if (frmt->flags & SPACE)
			ac[frmt->n[0]++] = ' ';
		frmt->out = &ac[frmt->n[0]];
		li2b(frmt, specifier);
	}
}
