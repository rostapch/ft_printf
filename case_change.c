/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   case_change.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 15:38:52 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/27 15:39:03 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			ft_islower(int c)
{
	return (('a' <= c && c <= 'z'));
}

int			ft_isupper(int c)
{
	return (('A' <= c && c <= 'Z'));
}

int			ft_tolower(int c)
{
	if (ft_isupper(c))
		return (c + 'a' - 'A');
	return (c);
}
