/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   spec_l_d_o_u.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 15:44:24 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/27 15:44:26 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	spec_ldou(t_format *frmt, va_list prsd_lst, char code, char *str)
{
	if (ft_strchr("DOU", code))
	{
		if (frmt->qual == l)
			frmt->qual = ll;
		else
			frmt->qual = l;
		code = ft_tolower(code);
		spec_di(frmt, prsd_lst, code, str);
		bin_o_u_x(frmt, prsd_lst, code, str);
	}
}
