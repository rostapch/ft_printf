/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   spec_undefined.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 15:45:03 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/27 15:45:05 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	spec_undef(t_format *frmt, char code, char *ac)
{
	int pad;

	frmt->out = &ac[frmt->n[0]];
	frmt->out[frmt->n[1]++] = code;
	if (SD_ZM && SD_N_NZ)
		frmt->nz[0] += pad;
}
