# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rostapch <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/10/27 15:40:44 by rostapch          #+#    #+#              #
#    Updated: 2017/10/27 15:40:46 by rostapch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf

LIB = libft/libft.a

SOURCES = \
		spec_bin_o_u_x.c \
		spec_c_lc.c \
		spec_di.c \
		spec_l_d_o_u.c \
		spec_n.c \
		spec_p.c \
		spec_s_ls.c \
		long2a_base.c \
		output.c \
		ft_printf.c \
		specifiers.c \
		longint2base.c \
		case_change.c \
		ull2a_base.c \
		spec_undefined.c \
		wcharstr2multibyte.c \
		wchar2multibyte.c \
		printf_input.c \
		some_stuff.c \
		parse_length_flags.c \

OBJECTS = $(SOURCES:.c=.o)

CC = gcc
CFLAGS = -Wall -Wextra -Werror

.PHONY: all re clean fclean tests

all: $(NAME)

$(NAME): lib $(OBJECTS)
	@ mv  $(LIB) ./$(NAME).a
	@ ar -sr $(NAME).a $(OBJECTS)

clean: lib_clean
	rm -f $(OBJECTS)
	rm -rf $(OBJECTS)

fclean: clean lib_fclean
	rm -f ./basic_test
	rm -f $(NAME).a

re: fclean all


lib:
	make -C libft -f Makefile

lib_re:
	make re -C libft -f Makefile

lib_clean:
	make clean -C libft -f Makefile

lib_fclean:
	make fclean -C libft -f Makefile

