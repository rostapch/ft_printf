/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_input.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 15:43:39 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/27 15:43:40 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

const char	*searc_arg(const char *str)
{
	const char *ret;

	ret = str;
	while (SKIP_RET)
		ret++;
	return (ret);
}

void		parse_arg(t_format *frmt, va_list parsed_lst, const char **s)
{
	(*s)++;
	parse_flags(s, frmt);
	get_width_arg(s, frmt, parsed_lst);
	get_prec_arg(s, frmt, parsed_lst);
	parse_length(s, frmt);
}

int			ft_printf_in(const char *str, va_list lst)
{
	t_format	frmt;
	const char	*s;
	char		temp[68];

	frmt.nchar = 0;
	while (1)
	{
		s = searc_arg(str);
		if (PUTN_RET || !(*s) || !(*(s + 1)))
			return (frmt.nchar);
		frmt.flags = 0;
		((*s) == '%') ? parse_arg(&frmt, lst, &s) : 0;
		putfld(&frmt, lst, *s, temp);
		output(&frmt, temp);
		if (ft_strchr("SC", *s)
			|| (ft_strchr("sc", *s) && frmt.qual == l))
			ft_strdel(&(frmt.out));
		str = s + 1;
	}
}
