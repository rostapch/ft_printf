/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   specifiers.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 15:45:11 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/27 15:45:13 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	putfld(t_format *frmt, va_list parsed_lst, char cd, char *str)
{
	frmt->n[0] = 0;
	frmt->nz[0] = 0;
	frmt->n[1] = 0;
	frmt->nz[1] = 0;
	frmt->n[2] = 0;
	frmt->nz[2] = 0;
	if (ft_strchr("nsSpdDioOuUxXcCb", cd))
	{
		spec_c(frmt, parsed_lst, cd, str);
		spec_di(frmt, parsed_lst, cd, str);
		spec_lc(frmt, parsed_lst, cd);
		spec_ls(frmt, parsed_lst, cd);
		spec_s(frmt, parsed_lst, cd);
		bin_o_u_x(frmt, parsed_lst, cd, str);
		spec_p(frmt, parsed_lst, cd, str);
		spec_ldou(frmt, parsed_lst, cd, str);
		spec_n(frmt, parsed_lst, cd);
	}
	else
	{
		spec_undef(frmt, cd, str);
	}
}
