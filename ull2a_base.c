/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ull2a_base.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 15:45:19 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/27 15:45:21 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*ull2ascii_base(t_ull value, int base)
{
	char	*res;
	int		len;
	t_ull	tmp;

	len = base_size(value, base);
	tmp = value;
	len -= ((long long)value < 0 && base == 10) ? 1 : 0;
	if (!((res = (char *)malloc(len + 1)) && base >= 2 && base <= 36))
		return (char *)0;
	res[len] = '\0';
	value == 0 ? res[0] = '0' : 0;
	while (tmp)
	{
		--len;
		res[len] = base_char(tmp % base);
		tmp /= base;
	}
	return (res);
}
