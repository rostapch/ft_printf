/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   spec_bin_o_u_x.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 15:43:59 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/27 15:44:00 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	bin_o_u_x(t_format *frmt, va_list prsd_lst, char code, char *str)
{
	if (ft_strchr("bouxX", code))
	{
		if (Q_L_LL)
			frmt->v.li = (frmt->qual == ll) ?
						va_arg(prsd_lst, t_ull) : va_arg(prsd_lst, t_ul);
		else if (Q_H_HH)
			frmt->v.li = (frmt->qual == hh) ?
						(unsigned char)va_arg(prsd_lst, int) :
						(unsigned short)va_arg(prsd_lst, int);
		else if (Q_J_Z)
			frmt->v.li = (frmt->qual == j) ?
						va_arg(prsd_lst, uintmax_t) : va_arg(prsd_lst, size_t);
		else
			frmt->v.li = (unsigned int)va_arg(prsd_lst, int);
		if (BOUX_OU && BOUX_SZ)
		{
			str[frmt->n[0]++] = '0';
			frmt->prec -= (code == 'o' && (frmt->v.li)) ? 1 : 0;
			if (ft_strchr("bxX", code))
				str[frmt->n[0]++] = code;
		}
		frmt->out = &str[frmt->n[0]];
		li2b(frmt, code);
	}
}
