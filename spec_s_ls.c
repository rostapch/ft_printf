/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   spec_s_ls.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 15:44:47 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/27 15:44:49 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	spec_s(t_format *frmt, va_list parsed_lst, char specifier)
{
	int		pad;

	if (specifier == 's' && frmt->qual != l)
	{
		frmt->out = va_arg(parsed_lst, char *);
		if (!(frmt->out))
		{
			frmt->out = "(null)";
		}
		frmt->n[1] = (int)ft_strlen(frmt->out);
		if (0 <= frmt->prec && frmt->prec < frmt->n[1])
			frmt->n[1] = frmt->prec;
		if (S_ZM && S_N_NZ)
			frmt->nz[0] += pad;
	}
	else if (specifier == 's')
		spec_ls(frmt, parsed_lst, 'S');
}

void	spec_ls(t_format *frmt, va_list parsed_lst, char specifier)
{
	wchar_t	*ws;
	char	*mbs;
	size_t	len;
	int		pad;

	if (specifier == 'S')
	{
		ws = va_arg(parsed_lst, wchar_t *);
		if (!(ws))
		{
			mbs = ft_strdup("(null)");
			len = ft_strlen(mbs);
		}
		else
			len = CHECK(frmt->prec, frmt->n[1]) ? wcharstr2mb(&mbs, ws) :
				wcharstrn2mb(&mbs, ws, (size_t)frmt->prec);
		if (len == (size_t)-1)
			return ;
		frmt->n[1] = (int)len;
		frmt->out = mbs;
		if (LS_ZM && LS_N_NZ)
			frmt->nz[0] += pad;
	}
}
