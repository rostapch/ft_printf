/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wcharstr2multibyte.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 15:45:43 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/27 15:45:45 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

size_t		wcharstr2mb(char **dest, wchar_t *wstr)
{
	char	*tmp[2];

	if (!dest)
		return ((size_t)-1);
	(*dest) = ft_strnew(0);
	while (*wstr)
	{
		tmp[0] = ft_strnew(4);
		if (-1 == wchar2multibyte(tmp[0], *wstr))
		{
			ft_strdel(dest);
			ft_strdel(&tmp[0]);
			return ((size_t)-1);
		}
		tmp[1] = *dest;
		*dest = ft_strjoin(tmp[1], tmp[0]);
		ft_strdel(&tmp[1]);
		ft_strdel(&tmp[0]);
		wstr++;
	}
	return (ft_strlen(*dest));
}

size_t		wcharstrn2mb(char **dest, wchar_t *wstr, size_t len)
{
	char	*tmp[2];
	int		ret;

	if (!dest)
		return ((size_t)-1);
	(*dest) = ft_strnew(0);
	ret = 0;
	while (len && *wstr && ((size_t)ret < len))
	{
		tmp[0] = ft_strnew(4);
		if (0 > (ret = wchar2multibyte(tmp[0], *wstr)))
		{
			ft_strdel(dest);
			ft_strdel(&tmp[0]);
			return ((size_t)-1);
		}
		tmp[1] = *dest;
		*dest = ft_strjoin(tmp[1], tmp[0]);
		ft_strdel(&tmp[1]);
		ft_strdel(&tmp[0]);
		wstr++;
		len -= ret;
	}
	return (ft_strlen(*dest));
}
