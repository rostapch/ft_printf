/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wchar2multibyte.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 15:45:33 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/27 15:45:35 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	conv2multibyte(int bts, wchar_t wchr, char *str)
{
	int		move;

	move = 0;
	while (++move < bts)
		str[move] = 0x80 | (wchr >> ((bts - move - 1) * 6) & 0x3F);
	--move;
	if (bts == 1)
		str[0] = FIRST_BYTE;
	else if (bts == 2)
		str[0] = SECOND_BYTE;
	else if (bts == 3)
		str[0] = THIRD_BYTE;
	else if (bts == 4)
		str[0] = FOURTH_BYTE;
	return (bts);
}

int			wchar2multibyte(char *str, wchar_t wchr)
{
	int		i;
	int		count;

	if (CHECK_WCHAR)
	{
		count = 1;
		i = 7;
		while (i < 22)
		{
			if (wchr >> i == 0)
				return (conv2multibyte(count, wchr, str));
			i += 4 + (i > 7);
			++count;
		}
	}
	return (-1);
}
