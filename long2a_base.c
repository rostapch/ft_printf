/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   long2a_base.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 15:42:45 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/27 15:42:49 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		base_size(t_ll ll, int base)
{
	int		ret;
	t_ull	temp;

	ret = 0;
	if (!ll)
		return (1);
	if (ll < 0 && base == 10)
		ret++;
	temp = ll;
	while (temp)
	{
		ret++;
		temp /= base;
	}
	return (ret);
}

char	base_char(int i)
{
	if (i > 36)
		return (0);
	if (i < 10)
		return ('0' + i);
	else
		return ('a' + (i - 10));
}

char	*ll2ascii_base(t_ll value, int base)
{
	char	*ret;
	int		len;
	t_ull	temp;

	len = base_size(value, base);
	temp = value;
	if (!((ret = (char *)malloc(len + 1)) && base >= 2 && base <= 36))
		return (char *)0;
	if (value < 0)
	{
		if (base == 10)
			ret[0] = '-';
	}
	ret[len] = '\0';
	value == 0 ? ret[0] = '0' : 0;
	while (temp)
	{
		--len;
		ret[len] = base_char(temp % base);
		temp /= base;
	}
	return (ret);
}
