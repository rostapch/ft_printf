/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_length_flags.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 15:43:28 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/27 15:43:30 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		get_numb(const char **s, va_list str)
{
	int numb;
	int sign;

	sign = (**s == '-') ? -1 : 1;
	(**s == '-') ? (*s)++ : 0;
	if (*(*s) == '*')
	{
		numb = va_arg(str, int);
		(*s)++;
		if (ft_isdigit(**s))
			numb = parse_number(s);
	}
	else
	{
		numb = parse_number(s);
		if (**s == '*')
		{
			numb = va_arg(str, int);
			(*s)++;
		}
	}
	return (sign * numb);
}

void	get_prec_arg(const char **s, t_format *frmt, va_list str)
{
	if (**s != '.')
		frmt->prec = -1;
	while (**s == '.')
	{
		(*s)++;
		frmt->prec = get_numb(s, str);
		if (frmt->prec < 0)
		{
			frmt->width = -frmt->prec;
			frmt->prec = -1;
			frmt->flags |= MINUS;
		}
	}
}

void	parse_length_2(const char **s, t_format *frmt)
{
	if (!ft_strchr("hl", (**s)))
		return ;
	if (**s == 'h' && frmt->qual == h)
		frmt->qual = hh;
	else if (frmt->qual == l)
		frmt->qual = ll;
	(*s)++;
}

void	parse_length(const char **s, t_format *frmt)
{
	frmt->qual = nolen;
	if (!ft_strchr("hljzL", (**s)))
		return ;
	if (**s == 'h')
		frmt->qual = h;
	else if (**s == 'l')
		frmt->qual = l;
	else if (**s == 'z')
		frmt->qual = z;
	else if (**s == 'j')
		frmt->qual = j;
	else
		frmt->qual = L;
	(*s)++;
	parse_length_2(s, frmt);
}

void	parse_flags(const char **s, t_format *frmt)
{
	const char *t;

	while ((t = ft_strchr("-+ #0", **s)) && !(*t == '0' && *(t + 1) == '.'))
	{
		frmt->flags |= g_flags_def[t - "-+ #0"];
		(*s)++;
	}
}
